let users = [];
let origenes = ["Barcelona", "Madrid", "Vigo", "Valencia", "Sevilla", "Girona"];
let destinos = ["Paris", "Viena", "Milan", "Venecia", "London", "Budapest"];

let fecha_ida = "";
let fecha_vuelta = "";
let city1 = "";
let city2 = "";

const diasDeLaSetmana = [
  'Diumenge',
  'Dilluns',
  'Dimarts',
  'Dimecres',
  'Dijous',
  'Divendres',
  'Dissabte'
];
const mesos = [
  'de Gener',
  'de Febrer',
  'de Març',
  'd\'Abril',
  'de Maig',
  'de Juny',
  'de Juliol',
  'd\'Agost',
  'de Setembre',
  'd\'Octubre',
  'de Novembre',
  'de Desembre'
];

document.addEventListener("DOMContentLoaded", (event) => {
  console.log("DOM fully loaded and parsed");
  //document.getElementById('greeting').innerText = '<%= message %>';
  crear_div_lista_origenes();
  crear_div_lista_destinos();
  const dateElement = document.getElementById("date");

  showGreeting();

  document.getElementById("register_button").addEventListener("click", show_register_form);
  document.getElementById("login_button").addEventListener("click", show_login_form);
  document.getElementById("submit_register_form").addEventListener("click", () => { add_user(users) });
  document.getElementById("submit_login_form").addEventListener("click", () => { validar_login_form(users) });
  document.getElementById("submit_reservation_form").addEventListener("click", validar_reservation_form);
  document.getElementById("logout_button").addEventListener("click", do_logout);

});
function checkCookie(cookieName) {

  let cookies = document.cookie;
  let cookieArray = cookies.split("; ");
  for (var i = 0; i < cookieArray.length; i++) {
    let cookie = cookieArray[i];
    let [name, value] = cookie.split("=");

    if (name === cookieName) {

      return value;
    }
  }

  return null;
}

function showGreeting() {
  let checkKuki = checkCookie("greeting");

  if (checkKuki !== "true") {
    document.getElementById('greeting').innerText = "Hello!  It's it your first time on the Vueling website";


    document.cookie = "greeting=true";
  } else {
    document.getElementById('greeting').innerText = '';
  }
}






function crear_div_lista_origenes() {

  conteiner_lista_origenes = document.getElementById("info_origenes");
  title = document.createElement("h3")
  title.innerHTML = "Our origins";
  conteiner_lista_origenes.appendChild(title);
  element_lista = document.createElement("ul")
  for (var i = 0; i < origenes.length; i++) {
    option = document.createElement("li");
    option.innerHTML = origenes[i];
    element_lista.appendChild(option);
  }
  conteiner_lista_origenes.appendChild(element_lista);
}

function crear_div_lista_destinos() {


  conteiner_lista_destinos = document.getElementById("info_destinos");

  title = document.createElement("h3")
  title.innerHTML = "Our destinations";
  conteiner_lista_destinos.appendChild(title);
  element_lista = document.createElement("ul")
  for (var i = 0; i < destinos.length; i++) {
    option = document.createElement("li");
    option.innerHTML = destinos[i];
    element_lista.appendChild(option);
  }
  conteiner_lista_destinos.appendChild(element_lista);
}



function updateTime(element) {
  var today = new Date();
  dia = diasDeLaSetmana[today.getDay()];
  date = today.getDate();
  mes = mesos[today.getMonth()];
  hour = today.getHours();
  min = today.getMinutes();
  sec = today.getSeconds()
  result = dia + " " + date + " " + mes + " de " + today.getFullYear() + ", " + hour + ":" + min + ":" + sec;
  element.innerHTML = result;

}


function add_user(users) {

  if (validar_register_form()) {
    let user = {
      usuario: document.getElementById("usuario").value,
      clave: document.getElementById("clave").value,
      email: document.getElementById("email").value,
      telefono: document.getElementById("telefono").value,
      dni: document.getElementById("dni").value
    };
    put_user_in_database(user);

    users.push(user);
    if (put_user_in_database) {
      show_login_form();
    }
    //console.log(users);


  };
}

async function put_user_in_database(user) {

  let putUserData = await sendToServer("POST", "/put_user_in_database", user);
  console.log(putUserData);
  if (putUserData.result_sql === true) {
    return true;
  } else {
    return false;
  }
}


function show_register_form() {
  document.getElementById("formulario_registro").style.display = "inline";//VOLVER AL ESTADO NORMAL SHOW REGISTRO AND HIDE RESERVATION
  document.getElementById("formulario_login").style.display = "none";
  document.getElementById("formulario_reservation").style.display = "none";
}

function show_login_form() {
  document.getElementById("formulario_registro").style.display = "none";
  document.getElementById("formulario_login").style.display = "inline";
  document.getElementById("formulario_reservation").style.display = "none";
}

function show_mensaje(id) {

  document.getElementById(id).style.display = "inline";

}

function show_mensaje_byClass(classs) { //funcion sin uso

  document.getElementsByClassName(classs).style.display = "inline";

}

function show_reservation_form() {
  document.getElementById("formulario_registro").style.display = "none";
  document.getElementById("formulario_login").style.display = "none";
  document.getElementById("formulario_reservation").style.display = "inline";
}



function validar_register_form() {
  console.log("en validar register form");
  let usuario = document.getElementById("usuario").value;
  validar_usuario(usuario);
  if (!validar_usuario(usuario)) {
    show_mensaje("mensaje_usuario");
  }

  let clave = document.getElementById("clave").value;
  validar_clave(clave);
  if (!validar_clave(clave)) {
    show_mensaje("mensaje_clave");
    //console.log("mensaje_clave");
  }

  let repetirclave = document.getElementById("repetirclave").value;
  validar_repetirclave(repetirclave, clave);
  if (!validar_repetirclave(repetirclave, clave)) {
    show_mensaje("mensaje_repetirclave");
    //console.log("mensaje_repetirclave");
  }

  let email = document.getElementById("email").value;
  validar_email(email);
  if (!validar_email(email)) {
    show_mensaje("mensaje_email");
    //console.log("mensaje_email");
  }

  let telefono = document.getElementById("telefono").value;
  //console.log(telefono);
  validar_telefono(telefono);
  if (!validar_telefono(telefono)) {
    show_mensaje("mensaje_telefono");
    //console.log("mensaje_repetirclave");
  }
  let dni = document.getElementById("dni").value;
  //console.log(dni);
  validar_dni(dni);
  if (!validar_dni(dni)) {
    show_mensaje("mensaje_dni");
  }
  console.log("todo valido");

  if (validar_usuario(usuario) &&
    validar_clave(clave) &&
    validar_repetirclave(repetirclave, clave) &&
    validar_email(email) &&
    validar_telefono(telefono)
    && validar_dni(dni)) {
    console.log("usuario validado");
    return true;
  };

}


async function validar_login_form(users) {
  //console.log("en validar login form");
  let login_usuario = document.getElementById("login_usuario").value;
  let login_clave = document.getElementById("login_clave").value;

  let login = {
    username: login_usuario,
    password: login_clave,
  }
  //console.log(login);
  //console.log(users);
  let login_result = await check_login_in_database(login);


  if (login_result) {
    // test
    //localStorage.setItem('login info', JSON.stringify(login));

    create_list_origins_and_destinations();

    register_button = document.getElementById("register_button");
    login_button = document.getElementById("login_button");
    register_button.style.display = "none";
    login_button.style.display = "none";
    logout_button = document.getElementById("logout_button");
    logout_button.style.display = "inline";


    show_reservation_form();

  }
  else {
    show_mensaje("mensaje_login");
  }

  /*   for (var i = 0; i < users.length; i++) {
      if (users[i].usuario == login_usuario && users[i].clave == login_clave) {
        console.log("Usuario existe");
  
        show_reservation_form();
      }
      else {
        show_mensaje("mensaje_login");
      } */


}




async function check_login_in_database(login) {
  //console.log("en check login in database");
  let checkUserData = await sendToServer("POST", "/check_login_in_database", login);
  //console.log(checkUserData);
  //console.log(checkUserData.result_sql);
  if (checkUserData.result_sql === true) {
    return true;
  } else {
    return false;
  }
}



async function create_list_origins_and_destinations() {
  //console.log("en create_list_origins_and_destinations");

  select_origins = document.getElementById("city_origin");


  select_destinations = document.getElementById("city_destination");

  //aqui es donde deberia poner los arrays sacados de bd
  //en vez de destinos y origenes

  /*   for (var i = 0; i < destinos.length; i++) {
      option = document.createElement("option");
      option.innerHTML = destinos[i];
      option.value = destinos[i];
      select_destinations.appendChild(option);
    }
  
  
    for (var i = 0; i < origenes.length; i++) {
      option2 = document.createElement("option");
      option2.innerHTML = origenes[i];
      option2.value = origenes[i];
      select_origins.appendChild(option2);
    } */
  let origins = [];
  let destinations = [];

  let info_origins_from_database = await sendToServer("GET", "/get_origins_from_database", "");
  //console.log(info_origins_from_database);
  //console.log(info_origins_from_database[0].city);

  for (var i = 0; i < info_origins_from_database.length; i++) {
    ciudad = info_origins_from_database[i].city;
    origins.push(ciudad);
  }
  //console.log(origins);

  let info_destinations_from_database = await sendToServer("GET", "/get_destinations_from_database", "");
  //console.log(info_destinations_from_database);
  //console.log(info_destinations_from_database[0].city);
  for (var i = 0; i < info_destinations_from_database.length; i++) {
    ciudad = info_destinations_from_database[i].city;
    destinations.push(ciudad);
  }
  //console.log(destinations);

  for (var i = 0; i < destinations.length; i++) {
    option = document.createElement("option");
    option.innerHTML = destinations[i];
    option.value = destinations[i];
    select_destinations.appendChild(option);
  }


  for (var i = 0; i < origins.length; i++) {
    option2 = document.createElement("option");
    option2.innerHTML = origins[i];
    option2.value = origins[i];
    select_origins.appendChild(option2);
  }

  /*   el resultado es 
  [
    RowDataPacket { city: 'Barcelona' },
    RowDataPacket { city: 'Madrid' },
    RowDataPacket { city: 'Vigo' },
    RowDataPacket { city: 'Valencia' },
    RowDataPacket { city: 'Sevilla' },
    RowDataPacket { city: 'Girona' }
  ]  */

}

function validar_reservation_form() {
  let reserva_origen = document.getElementById("city_origin").value;//- Origen (obligatòri, solament text)
  let reserva_destino = document.getElementById("city_destination").value;//- Destí (obligatòri, solament text)//console.log(reserva_origen);
  let reserva_ida = document.getElementById("reserva_ida").value;//	- Data anada (obligatòri, tipus data, format data)
  let reserva_vuelta = document.getElementById("reserva_vuelta").value;

  if (!validar_dates(reserva_ida, reserva_vuelta)) {
    show_mensaje("mensaje_fechas");
  }
  if (validar_origen(origenes, reserva_origen) &&
    validar_destino(destinos, reserva_destino) &&
    validar_dates(reserva_ida, reserva_vuelta)) {
    //console.log("Reservation is valid");

    city1 = reserva_origen;
    city2 = reserva_destino;
    fecha_ida = reserva_ida;
    fecha_vuelta = reserva_vuelta;

    pasajeros = parseInt(document.getElementById("pasajeros").value);
    formulario_pasajeros();

  };


}


function formulario_pasajeros() {

  pasajeros = parseInt(document.getElementById("pasajeros").value);
  //console.log(pasajeros);
  document.getElementById("formulario_reservation").style.display = "none";
  fieldset_pasajeros = document.createElement("fieldset");
  fieldset_pasajeros.className = "reserva_fieldset";
  fieldset_pasajeros.classList.add("p-2");
  form = document.createElement("form");
  form.classList.add("px-3");
  fieldset_pasajeros.appendChild(form);

  for (var i = 0; i < pasajeros; i++) {

    var div_data_container = document.createElement("div");
    div_data_container.classList.add("data-container");

    var div_nombre = document.createElement("div");
    div_nombre.classList.add("form-group");


    var label_nombre = document.createElement("label");
    label_nombre.innerHTML = "Passenger name:";

    var input_nombre = document.createElement("input");
    input_nombre.classList.add("form-control");
    input_nombre.classList.add("passenger");
    input_nombre.type = "text";
    input_nombre.value = "";
    input_nombre.id = "passenger_name";
    input_nombre.required = true;

    var message_error_nombre = document.createElement("div");
    message_error_nombre.className = "mensaje";
    message_error_nombre.innerHTML = "Nombre incorrecto";
    message_error_nombre.id = "mensaje_passenger";

    div_nombre.appendChild(label_nombre);
    div_nombre.appendChild(input_nombre);
    div_nombre.appendChild(message_error_nombre);
    div_data_container.appendChild(div_nombre);

    var div_dni = document.createElement("div");
    div_dni.classList.add("form-group");

    var label_dni = document.createElement("label");
    label_dni.innerHTML = "Dni:";

    var input_dni = document.createElement("input");
    input_dni.classList.add("form-control");
    input_dni.classList.add("dni");
    input_dni.type = "text";
    input_dni.value = "";
    input_dni.id = "passenger_dni";
    input_dni.required = true;

    var message_error_dni = document.createElement("div");
    message_error_dni.className = "mensaje";
    message_error_dni.innerHTML = "Dni incorrecto";
    message_error_dni.id = "mensaje_passenger_dni";

    div_dni.appendChild(label_dni);
    div_dni.appendChild(input_dni);
    div_dni.appendChild(message_error_dni);
    div_data_container.appendChild(div_dni);

    form.appendChild(div_data_container);


  }


  var div_button = document.createElement("div");
  div_button.classList.add("form-group", "mt-3");

  var button_enviar_passengers = document.createElement("button");
  button_enviar_passengers.classList.add("form-control");
  button_enviar_passengers.id = "submit_passengers_date";
  button_enviar_passengers.type = "button";
  button_enviar_passengers.innerHTML = "Enviar";

  div_button.appendChild(button_enviar_passengers);
  form.appendChild(div_button);

  formulario_pasajeros = document.getElementById("formulario_pasajeros");
  formulario_pasajeros.appendChild(fieldset_pasajeros);


  formulario_pasajeros.style.display = "inline";



  document.addEventListener("click", function (event) {
    if (event.target && event.target.id === "submit_passengers_date") {
      validar_passengers_info();
    }
  })

}


function validar_passengers_info() {
  //console.log("en validar pasajeros");

  let passengers_info = document.querySelectorAll(".passenger");
  let dni_info = document.querySelectorAll(".dni");

  let array_passengers_info = [];
  let array_dni_info = [];

  for (let i = 0; i < passengers_info.length; i++) {
    array_passengers_info.push(passengers_info[i].value);
  }

  for (let i = 0; i < dni_info.length; i++) {
    array_dni_info.push(dni_info[i].value);
  }

  passengers_validado = true;
  dni_validado = true;

  for (let i = 0; i < array_passengers_info.length; i++) {
    validar_usuario(array_passengers_info[i]);
    if (validar_usuario(array_passengers_info[i]) == false) {
      passengers_validado = false;
    }
  }

  if (passengers_validado == false) {
    {
      show_mensaje("mensaje_passenger");
    }
  }

  for (let i = 0; i < array_dni_info.length; i++) {
    validar_dni(array_dni_info[i]);
    if (validar_dni(array_dni_info[i]) == false) {
      dni_validado = false;
    }
  }

  if (dni_validado == false) {
    {
      show_mensaje("mensaje_passenger_dni");
    }
  }


  if ((passengers_validado == true) && (dni_validado == true)) {
    //print_reservas(array_passengers_info, array_dni_info);
    save_local_storage();



  }

};

function save_local_storage() {
  let containers = document.querySelectorAll('.data-container');
  let passengerDataArray = [];

  for (let i = 0; i < containers.length; i++) {

    let nameInput = containers[i].querySelector('.passenger');
    let dniInput = containers[i].querySelector('.dni');

    let passengerData = {
      name: nameInput.value,
      dni: dniInput.value
    };

    passengerDataArray.push(passengerData);
  };

 
  //console.log(passengerDataArray);
  localStorage.setItem('Passengers data', JSON.stringify(passengerDataArray));
  print_data(passengerDataArray);

}

function print_data(passengerDataArray) {
  document.getElementById("formulario_pasajeros").style.display = "none";
  //console.log("The passengers are ");
  document.getElementById("reserva").style.display = "inline";
  result = "Passengers: <br>";
  for (let i = 0; i < passengerDataArray.length; i++) {
    let persona = passengerDataArray[i];
    let persona_data = "Nombre: " + persona.name + ", DNI: " + persona.dni + "<br>";
    result += persona_data;
  }
  result = result + "Origen: " + city1 + "<br>Destino: " + city2 + "<br>Fecha ida: " + fecha_ida + "<br>Fecha vuelta: " + fecha_vuelta;


  document.getElementById("reserva").innerHTML = result;


}

//función anterior, ahora está sin uso 
function print_reservas(array_passengers_info, array_dni_info) {
  document.getElementById("formulario_pasajeros").style.display = "none";
  //console.log("The passengers are ");

  document.getElementById("reserva").style.display = "inline";

  datos_pasajeros = "";
  for (let i = 0; i < array_passengers_info.length; i++) {
    if (i > 0) {
      datos_pasajeros += ", ";
    }
    datos_pasajeros += array_passengers_info[i];
  }

  datos_dnis = "";
  for (let i = 0; i < array_dni_info.length; i++) {
    if (i > 0) {
      datos_dnis += ", ";
    }
    datos_dnis += array_dni_info[i];
  }


  result = "The passengers are: " + datos_pasajeros + "<br>Dnis: " + datos_dnis + "<br>Origen: " + city1 + "<br>Destino: " + city2 + "<br>Fecha ida: " + fecha_ida + "<br>Fecha vuelta: " + fecha_vuelta;


  document.getElementById("reserva").innerHTML = result;
}


async function do_logout() {
  var todasLasCookies = document.cookie;


  let logout = await sendToServer("GET", "/logout", "");
  logout_button.style.display = "none";
  adios = "Bye!"
  document.getElementById("reserva").innerHTML = adios;
}






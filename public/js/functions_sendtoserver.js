function sendToServer(method, path, data){
    return new Promise(function(resolve, reject){
        let xhr = new XMLHttpRequest();
        xhr.open(method, path);
/*         xhr.setRequestHeader('Content-Type', 'application/json');
        data = JSON.stringify(data);
        xhr.send(data); */
        if (data) {
            xhr.setRequestHeader('Content-Type', 'application/json');
            data = JSON.stringify(data);
            xhr.send(data);
        } else {
            xhr.send();
        }
        xhr.onload = function() {
            if (xhr.status != 200) { // analiza el estado HTTP de la respuesta
                reject(new Error(xhr.status));
            } else { // muestra el resultado
                resolve(JSON.parse(xhr.response));
            }
        };
    });
  }
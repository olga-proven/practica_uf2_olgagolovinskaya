
function validar_usuario(usuario) {//- Usuari (obligatòri, mínim 3 caràcters, màxim 10, solament text)
  var letters = /^[A-Za-z]+$/;
  if (usuario.length > 3 && usuario.length < 10 && usuario.match(letters)) {
    //console.log("username validado");
    return true
  }
  else {
    //console.log("username no validado");
    return false;
  }
}

function validar_clave(clave) {//- Clau (obligatòri, mínim 8 caràcters, obligatòri majúscula-minúscula-número)
  let mayuscula = false;
  let numero = false;
  let menuscula = false;
  let lenght = false;

  if (clave.length > 8) {
    lenght = true;
    //console.log("clave mas de 8")
  }

  for (i = 0; i < clave.length; i++) {
    simbolo = clave.charAt(i);
    //console.log(simbolo);
    if (Number.isInteger(parseInt(simbolo))) {
      numero = true;
      //console.log("encontrado un digito");
    }
    else if (simbolo == simbolo.toUpperCase()) {
      //console.log(simbolo, simbolo.toUpperCase());
      mayuscula = true;
      //console.log("encontrado una mayuscula");
    }
    else if (simbolo == simbolo.toLowerCase()) {
      menuscula = true;
      //console.log("encontrado una menuscula");
    }
  }
  //console.log(numero);
  //console.log(mayuscula, menuscula, numero, lenght);

  if ((numero === true) && (mayuscula === true) && (menuscula === true) && (lenght === true)) {
    //console.log("clave validado");
    return true;
  }
  else {
    //console.log("clave NO validado");
    return false;
  }
}

function validar_repetirclave(repetirclave, clave) {//- Repetir clau (obligatòri, igual al camp clau)

  if (repetirclave == clave) {
    //console.log("repetirclave validado");
    return true
  }
  else {
    //console.log("repetirclave NO validado");
    return false;
  }
}


function validar_email(email) {//- E-mail (obligatòri, format e-mail)
  if (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(email)) {
    //console.log("email validado");
    return true;
  } else {
    return false;
  }
}


function validar_telefono(telefono) {//- Telèfon (obligatòri, 9 xifres, comença per 93 o 6)
  if (/^(93|6)\d{8}$/.test(telefono)) {
    //console.log("telefono validado");
    return true;
  } else {
    //console.log("telefono no validado");
    return false;
  }
}

function validar_dni(dni) {//	- DNI (opcional, validació DNI)
  let letras = "TRWAGMYFPDXBNJZSQVHLCKE";
  //console.log(dni);
  //console.log(dni.length);
  if ((dni.length == 9)) {//&& (/^[0-9XYZ]$/.test(dni))
    //07246561C
    //console.log("dni es  9")
    let numeros = parseInt(dni.slice(0, 8));
    //console.log(numeros);
    let letrapos = numeros % 23;
    //console.log(letrapos);
    //console.log(letras[letrapos]);
    if (letras[letrapos] == dni[dni.length - 1]) {
      //console.log("dni  validado");
      return true;
    }
    else {
      //console.log("dni  NOvalidado");
      return false;
    }
  } else {
    //console.log("dni NO validado");
    return false;
  }
}



function validar_origen(origenes, reserva_origen) { //- S'ha de comprovar que l'origen està al nostre array d'orígens.
  //console.log(origenes);
  //console.log(reserva_origen);
  if (validar_text(reserva_origen) && origenes.includes(reserva_origen)) {
    return true;
  }
  else {
    return false
      ;
  }
}

function validar_destino(destinos, reserva_destino) {//- S'ha de comprovar que el destí està al nostre array de destins.
  if (validar_text(reserva_destino) && destinos.includes(reserva_destino)) {
    return true;
  }
  else {
    return false
      ;
  }
}

function validar_text(words) {
  let letters = /^[A-Za-z]+$/;
  //console.log(words);
  if (words.match(letters)) {
    return true;
  }
  else {
    return false;
  }
}

function validar_dates(date1, date2) {        //- S'ha de comprovar que la data de tornada és igual o posterior a la data d'anada
  let dateone = new Date(date1).getTime();
  let datetwo = new Date(date2).getTime();
  if (dateone < datetwo) {	//- S'ha de comprovar que les dues dates són iguals o posteriors al dia d'avui.
    return true
  }
  else {
    return false;
  }
}


const express = require('express');
const app = express();
const port = 3000;
app.use(express.json());
const cookieParser = require('cookie-parser');

let sessions_cookies = {};
const crypto = require('crypto');
const secretKey = 'abrakadabra';



app.use(express.static('./public'));
app.use(cookieParser());

app.get('/', (req, res) => {
  res.sendFile('public/index.html');
}); 


app.post('/put_user_in_database', (req, res) => {
  console.log(req.params);
  console.log(req.query);
  console.log(req.body);
  let username = req.body.usuario;
  //let password = req.body.clave;
  let password = crypto.createHash('md5').update(req.body.clave).digest('base64');
  let email = req.body.email;

  console.log(password);
  let telefono = req.body.telefono;
  let dni = req.body.dni;
  const sql = "INSERT INTO usuarios (username, password, email, telefono, dni) VALUES (?, ?, ?, ?, ?)";
  const params = [username, password, email, telefono, dni];

  db(sql, params, function (results) {
    console.log(results);
    if (results.length === 1) {
      res.json({
        result_sql: true
      });
    } else {
      res.json({
        result_sql: false
      });
    }
  });
});




app.post('/check_login_in_database', (req, res) => {
  console.log(req.params);
  console.log(req.query);
  console.log(req.body);
  let username = req.body.username;
  //let password = req.body.password;
  let password = crypto.createHash('md5').update(req.body.password).digest('base64');

  const sql = "SELECT `username`, `password` FROM usuarios WHERE username = ? AND password = ?;";

  const params = [username, password];



  db(sql, params, function (results) {
    console.log(results);
    if (results.length === 1) {

      let sessionID2 = crypto.randomUUID();
      console.log(sessionID2);
      const firma = crypto.createHmac('sha256', secretKey)
        .update(sessionID2).digest('base64');
      let variable = sessionID2 + '.' + firma;
      console.log(sessionID2 + '.' + firma);
      let expiration_term = new Date(Date.now() + 8 * 60 * 60 * 1000);

      res.cookie('sessionID', variable, {
        //maxAge: 60 * 1,
        expires: expiration_term,
        httpOnly: true

      });
      //guardar cookie info en el objeto 
      sessions_cookies[variable] = { //variable es sessions id 
        user: username,               //guardo username
        expire_date: expiration_term    //guardo expiration term
      };
      console.log("variable global com la kuki guardad :", sessions_cookies);

      res.json({
        result_sql: true

      });
    } else {
      res.json({
        result_sql: false
      });
    }
  });
});


/* app.get('/provaCookie', (req, res) => {
  console.log('Cookies: ', req.cookies);

  res.cookie('sessionID', '3798349bjcfb3648374', {
    //maxAge: 60 * 1,
    expires: new Date(Date.now() + + 8 * 60 * 60 * 1000),
    httpOnly: true
  })

  res.json({ res: 'cookie creada' });
}); */


app.get('/get_origins_from_database', (req, res) => {
  console.log(req.params);
  console.log(req.query);
  console.log(req.body);
  const sql = "SELECT city FROM origenes;";
  const params = [];
  db(sql, params, function (results) {
    console.log(results);
    res.json(results);


  });
});


app.get('/get_destinations_from_database', (req, res) => {

  console.log(req.params);
  console.log(req.query);
  console.log(req.body);
  const sql = "SELECT city FROM destinos;";
  const params = [];
  db(sql, params, function (results) {
    console.log(results);
    res.json(results);

  });
});

/*   function check_sql_results(results, res) {
    console.log(results);
    console.log(results.length);
  
    if (results.length === 1) {
      res.json({
        result_sql: true
      });
    } else {
      res.json({
        result_sql: false
      });
    }
  } */

app.get('/logout', (req, res) => {

  console.log(req.params);
  console.log(req.query);
  console.log(req.body);
  console.log("Req kukis:", req.cookies);
  console.log("Variable global session kukis desde logout:" , sessions_cookies);
  console.log(req.cookies.sessionID);
  console.log(sessions_cookies[req.cookies.sessionID])
  if (sessions_cookies[req.cookies.sessionID] != undefined){
    res.cookie('sessionID', req.cookies.sessionID, {
      maxAge: -1
     
    });
    res.json("session id deleted")


  }

});



function db(sql, params, callback) {
  var mysql = require('mysql');
  var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'm06'
  });

  connection.connect();

  connection.query(sql, params, function (error, results, fields) {
    if (error) throw error;
    console.log(results);
    callback(results);
  });

  connection.end();
}



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})



